import sqlite3
import json
from BeautifulSoup import BeautifulSoup
import mechanize
import re
import os


class Card(object):

    DB_PATH = 'cards.db'
    DB_TABLE_NAME = 'cards'

    def __init__(self):
        self.url = None
        self.name = None
        self.type_ = None
        self.cost = None
        self.oracle_text = None
        self.flavor_text = None
        self.power = None
        self.toughness = None
        self.illustrator = None
        self.rarity = None
        self.abilities = []
        self.edition = None
        self.pricing = {
            'L': None,
            'M': None,
            'H': None
        }
        self.image_name = None

        self.other_side = None

    def __init__(self, url, update):

        # See if Card already exists in the DB
        db = sqlite3.connect(Card.DB_PATH)
        cursor = db.cursor()
        cursor.execute("""SELECT json_card from %s WHERE url = ?""" %
                Card.DB_TABLE_NAME, (url,))
        card = cursor.fetchone()

        if (card is not None and not update):
            card_dict = json.loads(card[0])
            self.url = card_dict['url']
            self.name = card_dict['name']
            self.type_ = card_dict['type_']
            self.cost = card_dict['cost']
            self.oracle_text = card_dict['oracle_text']
            self.flavor_text = card_dict['flavor_text']
            self.power = card_dict['power']
            self.toughness = card_dict['toughness']
            self.illustrator = card_dict['illustrator']
            self.rarity = card_dict['rarity']
            self.abilities = card_dict['abilities']
            self.edition = card_dict['edition']
            self.pricing = card_dict['pricing']
            self.image_name = card_dict['image_name']
            self.other_side = card_dict['other_side']
            self.url = card_dict['url']

        else:
            self.url = None
            self.name = None
            self.type_ = None
            self.cost = None
            self.oracle_text = None
            self.flavor_text = None
            self.power = None
            self.toughness = None
            self.illustrator = None
            self.rarity = None
            self.abilities = []
            self.edition = None
            self.pricing = {
                'L': None,
                'M': None,
                'H': None
            }
            self.image_name = None

            self.other_side = None
            self.url = url
            soup = BeautifulSoup(mechanize.urlopen(url))

            # Find name
            name_table = soup.findAll('table')[3]
            card_name_a = name_table.findAll('a')[0]
            self.name = card_name_a.contents[0]

            # Find power & toughness
            type_p = name_table.findAll('p')[0]
            try:
                self.type_, self.cost = [
                    element.strip()
                    for element
                    in type_p.contents[0].split(',')
                ]
            except ValueError:
                self.type_ = type_p.contents[0]

            # See about extracting power/defense from the type
            re_match = re.search(r'(?P<type>.*?)(?: (?P<power>[*0-9]{1,})/(?P<toughness>[*0-9]{1,})|$)', self.type_)
            self.type_ = re_match.group('type').strip()
            self.power = re_match.group('power')
            self.toughness = re_match.group('toughness')

            # Find abilities...
            abilities = soup.findAll('p', {'class': 'ctext'})[0]
            self.abilities = abilities.next.text

            # Now the flavor text...
            flavor = abilities.findNextSibling()
            self.flavor = flavor.next.text

            # And the illustrator...
            illustrator = flavor.findNextSibling()
            self.illustrator = re.search('Illus. (?P<illustrator>.*)', illustrator.text).group('illustrator')

            # The legalities and rulings...
            legalities_and_rulings = illustrator.findNextSibling()

            # Now find the rarity...
            editions = soup.findAll('b', text='Editions:')
            self.editions = [e.findNext('b').text for e in editions]
            #for edition in editions:
            #    print edition.findNext('b').text

            img_url = soup.find('img', {'alt': self.name})['src']
            self.image_name = os.path.join('imgs', self.name + ".jpg")
            with open(self.image_name, 'w+') as card_img:
                card_img.write(mechanize.urlopen(img_url).read())

            print self.url + " updated!"
            # print unicode(self)
            # print '    Abilities: ', self.abilities
            # print '    Flavor: ', self.flavor
            # print '    Illustrator: ', self.illustrator
            # print '    Editions: '
            # print '           - ', '\n         - '.join(self.editions)
            # print '\n'
            # #print legalities_and_rulings

    def save(self):
        '''Save a card'''
        db = sqlite3.connect(Card.DB_PATH)

        card_json = json.dumps(self.__dict__)
        db.execute("""INSERT OR REPLACE INTO %s (url, name, json_card)
                      VALUES (:url, :name, :json)""" % Card.DB_TABLE_NAME,
                 {"url": self.url,
                  "name": re.escape(self.name),
                  "json": card_json})
        db.commit()
        db.close()

    def __unicode__(self):
        return "%s (%s)%s\n   Cost: %s" % (self.name, self.type_, ' -- %s/%s' % (self.power, self.toughness) if self.power is not None else '', self.cost)
    #def __repr__(self):
    #    test = "%s (%s)%s\n   Cost: %s" % (self.name, self.type_,
    #                                    ' -- %s/%s' % (self.power, self.toughness) if self.power is not None else '',
    #                                    self.cost)

    @staticmethod
    def create_db():
        # Connect to and init table
        db = sqlite3.connect(Card.DB_PATH)
        c = db.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS %s (
                        url TEXT PRIMARY KEY,
                        name TEXT,
                        json_card text,
                        created_at DATETIME DEFAULT CURRENT_TIMESTAMP)
                  ''' % Card.DB_TABLE_NAME)

        db.commit()
        c.close()
        db.close()

    @staticmethod
    def getKnownCardSet():
        '''Get Set of Known Card Urls'''
        try:
            db = sqlite3.connect(Card.DB_PATH)
            cursor = db.cursor()
            cursor.execute("SELECT url from " + Card.DB_TABLE_NAME)
            card_urls = cursor.fetchall()
            return set([card_url[0] for card_url in card_urls])
        except Exception as e:
            return set()
