# Lib Imports
from BeautifulSoup import BeautifulSoup
import cookielib
import mechanize
import re
import argparse

# Local Imports
from Card import Card

SITE_ROOT = 'http://www.magiccards.info'

parser = argparse.ArgumentParser(description="Utility to Scrape and Store magiccards.info Card Data")

parser.add_argument('-u', '--uri', 
                    help="Specify specific URL of Card, Ex. '/isd/121/en.html'", 
                    required=False)

parser.add_argument('--update', action="store_const",
                    help="Force Utility to update information from magiccards.info before display",
                    const=True,
                    default=False,
                    required=False)

args = None


def get_set_list(list_type, update):
    '''Get a list of all cards not yet saved.'''
    site_map_html = mechanize.urlopen(SITE_ROOT + "/sitemap.html")
    soup = BeautifulSoup(site_map_html)

    try:
        english_expansions = soup.findAll(text=list_type)[0].next
    except Exception, e:
        print 'Set not found'
        return False

    english_expansions = english_expansions.findAll('a')
    for expansion in english_expansions:
        get_card_list(expansion['href'], update)


def get_card_list(url, update):
    '''Get list of card urls for a given set url'''
    soup = BeautifulSoup(mechanize.urlopen(SITE_ROOT + url))
    cards = soup.findAll('tr', {'class': re.compile('even|odd')})
    card_urls = set([SITE_ROOT + card.find('a')['href'] for card in cards])

    for card_url in card_urls:
        c = Card(card_url, update)
        print unicode(c)
        c.save()


def run():
    '''Main function. This initiates the scrape.'''
    # make sure the card table is created
    Card.create_db()

    if args['uri'] is not None:
        c = Card(SITE_ROOT + args['uri'],args['update'])
        print unicode(c)
    else:
        get_set_list('Expansions', args['update'])
        get_set_list('Core Sets', args['update'])


if __name__ == '__main__':
    args = vars(parser.parse_args())
    run()
